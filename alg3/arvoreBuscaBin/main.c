#include "libBST.h"
#include <stdio.h>
#include <stdlib.h>

int main()
{
    struct arvore* arvore = criaArvore();
    int dado;
    struct nodoArvore *novo;
    printf("dado: ");
    scanf("%d", &dado);
    while (dado != -1)
    {
        novo = criaNodo(dado);
        insere(novo, arvore);
        emOrdem(arvore);
        printf("dado: ");
        scanf("%d", &dado);
    }

    int op;
    while(1)
    {
        printf ("1 busca, 2 remove, 3 printa, 4 sucessor, 5 precessor ");
        scanf("%d", &op);
        if (op == 1)
        {
            printf("buscar: ");
            scanf("%d", &dado);
            novo = busca(dado, arvore);
            printf ("nodo: %d ,", novo->dado);
            if (novo->fe) printf("fe: %d ,", novo->fe->dado);
            if (novo->fd) printf("fd: %d ,", novo->fd->dado);
            if (novo->pai) printf("pai: %d\n", novo->pai->dado);
        }
        if (op == 2)
        {
            printf("remover: ");
            scanf("%d", &dado);
            novo = busca(dado, arvore);
            removeNodo(novo, arvore);
            free(novo);
            novo = NULL;
        }
        if (op == 3)
        {
            emOrdem(arvore);
        }
        if (op == 4)
        {
            printf("sucessor de: ");
            scanf("%d", &dado);
            novo = busca(dado, arvore);
            int sucessor = (achaSucessor(novo))->dado;
            printf("%d\n", sucessor);
        }
        if (op == 5)
        {
            printf("precessir de: ");
            scanf("%d", &dado);
            novo = busca(dado, arvore);
            int precessor = (achaPrecessor(novo))->dado;
            printf("%d\n", precessor);
        }
        if (op == 6) break;
    }

    destroiArvore(arvore);
}