#include "libBST.h"
#include <stdlib.h>
#include <stdio.h>



struct arvore *criaArvore()
{
    struct arvore* novaArvore = malloc(sizeof(struct arvore));
    novaArvore->raiz = NULL;
    return novaArvore;
}

int eh_vazia(struct arvore *arvore)
{
    return (arvore->raiz == NULL);
}

struct nodoArvore *criaNodo(int dado)
{
    struct nodoArvore *novoNodo = malloc(sizeof(struct nodoArvore));
    novoNodo->dado = dado;
    novoNodo->fd = NULL;
    novoNodo->fe = NULL;
    novoNodo->pai = NULL;
    return novoNodo;
}

struct nodoArvore *buscaRecursiva(int dado, struct nodoArvore* nodo)
{
    if ((nodo == NULL) || (nodo-> dado == dado)) return nodo;
    if (dado > nodo->dado) return buscaRecursiva(dado, nodo->fd);
    return buscaRecursiva(dado, nodo->fe);
}

struct nodoArvore *busca(int dado, struct arvore* arvore)
{
    if (eh_vazia(arvore)) return NULL;
    return buscaRecursiva(dado, arvore->raiz);
}

void insere(struct nodoArvore *novoNodo, struct arvore *arvore)
{
    if (eh_vazia(arvore))
    { 
        arvore->raiz = novoNodo;
        return;
    }
    
    struct nodoArvore *atual = arvore->raiz;
    struct nodoArvore *pai = NULL;
    while (atual != NULL)
    {
        pai = atual;
        if (novoNodo->dado > atual->dado)
            atual = atual->fd;
        else
            atual = atual->fe;
    }
    novoNodo->pai = pai;

    if (novoNodo->dado > pai->dado)
    {
        pai->fd = novoNodo;
        return;
    }
    pai->fe = novoNodo;
}

void emOrdemRecursivo(struct nodoArvore *nodo)
{
    if (nodo == NULL) return;
    emOrdemRecursivo(nodo->fe);
    printf("%d ", nodo->dado);
    emOrdemRecursivo(nodo->fd);
}

void emOrdem(struct arvore* arvore)
{
    if (eh_vazia(arvore)) return;
    emOrdemRecursivo(arvore->raiz);
    printf("\n");
}

struct nodoArvore* minimoRecursivo(struct nodoArvore* nodo)
{
    if (nodo->fe == NULL) return nodo;
    return minimoRecursivo(nodo->fe);
}

struct nodoArvore *minimo(struct arvore *arvore)
{
    if (eh_vazia(arvore)) return NULL;
    return minimoRecursivo(arvore->raiz);
}

struct nodoArvore* maximoRecursivo(struct nodoArvore *nodo)
{
    if (nodo->fd == NULL) return nodo;
    return maximoRecursivo(nodo->fd);
}

struct nodoArvore* maximo(struct arvore *arvore)
{
    if (eh_vazia(arvore)) return NULL;
    return maximoRecursivo(arvore->raiz);
}

struct nodoArvore *achaPrecessor(struct nodoArvore *nodo)
{
    if (nodo->fe != NULL) return maximoRecursivo(nodo->fe);

    struct nodoArvore* aux = nodo;
    while(aux != NULL)
    {
        if (aux == aux->pai->fd) return aux->pai;
        aux = aux->pai;
    } 
    return NULL;
}

struct nodoArvore *achaSucessor(struct nodoArvore *nodo)
{
    if (nodo->fd != NULL) return minimoRecursivo(nodo->fd);

    struct nodoArvore *aux = nodo;
    while (aux != NULL)
    {
        if (aux == aux->pai->fe) return aux->pai;
        aux = aux->pai;
    }
    return NULL;
}

void removeNodo(struct nodoArvore *nodo, struct arvore *arvore)
{   
    //eh folha
    if ((nodo->fd == NULL) && (nodo->fe == NULL))
    {
        //eh raiz
        if (nodo == arvore->raiz) 
        {
            arvore->raiz = NULL;
            return;
        }
        //nao eh raiz
        if (nodo->pai->fd == nodo) nodo->pai->fd = NULL;
        else nodo->pai->fe = NULL;
        nodo->pai = NULL;
        return;
    }

    //soh tem um filho
    if ((nodo->fd == NULL) ^ (nodo->fe == NULL))
    {
        struct nodoArvore *filhoSubs;
        if (nodo->fd != NULL) filhoSubs = nodo->fd;
        else filhoSubs = nodo->fe;

        if (nodo == arvore->raiz) arvore->raiz = filhoSubs;
        else if (nodo->pai->fd == nodo) nodo->pai->fd = filhoSubs;
        else nodo->pai->fe = filhoSubs; 
        filhoSubs->pai = nodo->pai;

        nodo->pai = NULL;
        nodo->fd = NULL;
        nodo->fe = NULL;
        return;
    }

    //tem os dois filhos
    struct nodoArvore *sucessor = achaSucessor(nodo);

    //acha sucessor que nao tem dois filhos
    while ((sucessor->fd != NULL) && (sucessor->fd != NULL))
    {
        sucessor = achaSucessor(sucessor);
    }
    printf("sucessor : %d ", sucessor->dado);

    //remove o sucessor
    removeNodo(sucessor, arvore);

    //copia dados do sucessor para o nodo
    if (nodo->pai == NULL) arvore->raiz = sucessor;
    else if (nodo->pai->fd == nodo) nodo->pai->fd = sucessor;
    else nodo->pai->fe = sucessor;
    sucessor->fd = nodo->fd;
    sucessor->fe = nodo->fe;
    sucessor->pai = nodo->pai;

    nodo->fd->pai = sucessor;
    nodo->fe->pai = sucessor;
    nodo->fd = NULL;
    nodo->fe = NULL;
    nodo->pai = NULL;
}


void destroiRecursivo(struct nodoArvore *nodo, struct arvore *arvore)
{
    printf("tentando liberar %d\n", nodo->dado);
    if (nodo->fd != NULL) destroiRecursivo(nodo->fd, arvore);
    if (nodo->fe != NULL) destroiRecursivo(nodo->fe, arvore);

    printf("liberando %d\n", nodo->dado);
    removeNodo(nodo, arvore);
    free(nodo);
    nodo = NULL;
    return; 
}

void destroiArvore(struct arvore* arvore)
{
    if (eh_vazia(arvore))
    {
        free(arvore);
        arvore = NULL;
        return;
    }
    destroiRecursivo(arvore->raiz, arvore);
    free(arvore);
    arvore = NULL;
    return;
}