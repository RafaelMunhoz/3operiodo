#ifndef LIBBST_H
#define LIBBST_H
#include <stdlib.h>

struct nodoArvore 
{
    int dado;
    struct nodoArvore *fd;
    struct nodoArvore *fe;
    struct nodoArvore *pai;
};

struct arvore
{
    struct nodoArvore *raiz;
};


struct arvore *criaArvore();

int eh_vazia(struct arvore *arvore);

struct nodoArvore *criaNodo(int dado);

struct nodoArvore *busca(int dado, struct arvore* arvore); 

void insere(struct nodoArvore *novoNodo, struct arvore *arvore);

void emOrdem(struct arvore* arvore);

struct nodoArvore *minimo(struct arvore *arvore);

struct nodoArvore* maximo(struct arvore *arvore);

struct nodoArvore *achaPrecessor(struct nodoArvore *nodo);

struct nodoArvore *achaSucessor(struct nodoArvore *nodo);

void removeNodo(struct nodoArvore *removido, struct arvore *arvore);

void destroiArvore(struct arvore *arvore);


#endif