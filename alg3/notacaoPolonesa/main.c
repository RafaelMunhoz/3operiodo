#include <stdio.h>
#include "lib.h"


int main()
{
    struct nodo *raiz = criaNodo();
    leOperacao(raiz);
    printf("Pre ordem: ");
    preOrdem(raiz);
    printf("\nEm ordem: ");
    emOrdem(raiz);
    printf("\nPós ordem: ");
    posOrdem(raiz);
    float result = resolve(raiz);
    printf("\nResultado: %.5f\n", result);
    liberaMemoria(raiz);
}