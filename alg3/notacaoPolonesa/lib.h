#include <stdio.h>
#define MAX 10

struct nodo
{
    char* dado;
    struct nodo* fd;
    struct nodo* fe;
    struct nodo* pai;
};

void emOrdem(struct nodo*);

void preOrdem(struct nodo*);

void posOrdem(struct nodo*);

struct nodo* criaNodo();

void leOperacao(struct nodo*);

float resolve(struct nodo *atual);

void liberaMemoria(struct nodo* atual);