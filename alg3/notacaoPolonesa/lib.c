#include <stdio.h>
#include <stdlib.h>
#include "lib.h"


void emOrdem(struct nodo* atual)
{
    if (atual == NULL) return;
    emOrdem(atual->fe);
    printf("%s ", atual->dado);
    emOrdem(atual->fd);
}

void preOrdem(struct nodo* atual)
{
    if (atual == NULL) return;
    printf("%s ", atual->dado);
    preOrdem(atual->fe);
    preOrdem(atual->fd);
}

void posOrdem(struct nodo* atual)
{
    if (atual == NULL) return;
    posOrdem(atual->fe);
    posOrdem(atual->fd);
    printf("%s ", atual->dado);
}

struct nodo* criaNodo()
{
    struct nodo *novo = malloc(sizeof(struct nodo));
    novo->fe = NULL;
    novo->fd = NULL;
    novo->dado = NULL;
    return novo;
}

int ehOperando(char *str)
{
    return ((str[0] >= '0') && (str[0] <= '9'));
}


// / - * 5.3 25.7 * 89 11.36 * 3 21

void leOperacao(struct nodo *atual)
{
    //le dado
    char* lido = malloc(MAX * (sizeof(char)));
    scanf("%s", lido);
    atual->dado = lido;

    if (!ehOperando(atual->dado))
    {
        //le operando da esquerda
        struct nodo* filhoEsq = criaNodo();
        atual->fe = filhoEsq;
        leOperacao(atual->fe);

        //le operando da direita
        struct nodo* filhoDir = criaNodo();
        atual->fd = filhoDir;
        leOperacao(atual->fd);
    }
    return;
}

float resolve(struct nodo *atual)
{
    if (ehOperando(atual->dado)) return atof(atual->dado);
    float opEsq = resolve(atual->fe);
    float opDir = resolve(atual->fd);

    if (atual->dado[0] == '*') return opEsq * opDir;
    if (atual->dado[0] == '/') return opEsq / opDir;
    if (atual->dado[0] == '+') return opEsq + opDir;
    if (atual->dado[0] == '-') return opEsq - opDir;
}

void liberaMemoria(struct nodo* atual)
{
    if (atual == NULL) return;
    liberaMemoria(atual->fe);
    liberaMemoria(atual->fd);
    free(atual->dado);
    free(atual);
}


