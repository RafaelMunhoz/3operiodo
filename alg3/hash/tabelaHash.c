#include "tabelaHash.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void matarProgramaFaltaMemoria() {
    fputs("Falha ao alocar memoria.\n", stderr);
    exit(1);
}

struct aluno* getAluno1() {
    struct aluno* retorno = malloc(sizeof(struct aluno));
    if (!retorno) matarProgramaFaltaMemoria();
    retorno->nome = malloc(sizeof("Rafael Munhoz da Cunha Marques"));  // sizeof conta o \0
    if (!retorno->nome)
        matarProgramaFaltaMemoria();
    retorno->nomeDinf = malloc(sizeof("rmcm22"));
    if (!(retorno->nomeDinf))
        matarProgramaFaltaMemoria();

    strcpy(retorno->nome, "Rafael Munhoz da Cunha Marques");
    strcpy(retorno->nomeDinf, "rmcm22");
    retorno->grr = 20224385;

    return retorno;
}

struct aluno* getAluno2() {
    return NULL;
}

void imprimirDadosAlunos() {
    struct aluno* aluno = getAluno1();

    printf("Trabalho de %s\n", aluno->nome);
    printf("Login Dinf %s\n", aluno->nomeDinf);
    printf("GRR %u\n\n", aluno->grr);

    free(aluno->nome);
    free(aluno->nomeDinf);
    free(aluno);

    aluno = getAluno2();
    if (!aluno) return;

    printf("...E... \n\n");
    printf("Trabalho de %s\n", aluno->nome);
    printf("Login Dinf %s\n", aluno->nomeDinf);
    printf("GRR %u\n\n", aluno->grr);

    free(aluno->nome);
    free(aluno->nomeDinf);
    free(aluno);
}

struct nodoHash** gerarTabelaHash(size_t tamTabela)
{
    struct nodoHash** tabela = malloc(tamTabela * sizeof(struct nodoHash*));
    if (!tabela)
        matarProgramaFaltaMemoria();
    return tabela;
}

int hash(size_t tamTabela, int chave)
{
    return abs(chave) % tamTabela;
}

struct nodoHash* criaNodo(int chave)
{
    struct nodoHash* novo = malloc(sizeof(struct nodoHash));
    if (!novo)
        matarProgramaFaltaMemoria();
    novo->chave = chave;
    novo->prox = NULL;
    novo->ant = NULL;
    return novo;
}

struct nodoHash* inserir(struct nodoHash* tabelaHash[], size_t tamTabela, int chave)
{
    struct nodoHash *novoNodo = criaNodo(chave);
    int pos = hash(tamTabela, chave);

    //se nao deu colisao, soh insere
    if (tabelaHash[pos] == NULL)
        tabelaHash[pos] = novoNodo;

    //se deu, checa duplicata e insere no fim da lista
    else {
        struct nodoHash* aux = tabelaHash[pos];
        struct nodoHash* ant;
        while (aux) {
            if (aux->chave == chave) {
                free(novoNodo);
                return NULL;
            }
            ant = aux;
            aux = aux->prox;
        }
        ant->prox = novoNodo;
        novoNodo->ant = ant;
    }

    return novoNodo;
}

void liberarTabelaHash(struct nodoHash* tabelaHash[], size_t tamTabela)
{
    struct nodoHash* aux;
    for (int i = 0; i < tamTabela; i++) {
        while (tabelaHash[i]) {
            aux = tabelaHash[i];
            tabelaHash[i] = tabelaHash[i]->prox;
            free(aux);
        }    
    }
    free(tabelaHash);
}

void imprimirTabelaHash(struct nodoHash* tabelaHash[], size_t tamTabela)
{
    struct nodoHash* aux;
    for (int i = 0; i < tamTabela; i++) {
        printf("%d ", i);
        aux = tabelaHash[i];
        while (aux) {
            printf("[%d] -> ", aux->chave);
            aux = aux->prox;
        }
       printf("NULL\n");
    }
}

struct nodoHash* buscar(struct nodoHash* tabelaHash[], size_t tamTabela, int chave)
{
    int pos = hash(tamTabela, chave);
    struct nodoHash* aux = tabelaHash[pos];
    while (aux) {
        if (aux->chave == chave)
            return aux;
        aux = aux->prox;
    }
    return NULL;
}

void excluir(struct nodoHash* tabelaHash[], size_t tamTabela, struct nodoHash* nodo)
{
    if (nodo->ant)
        nodo->ant->prox = nodo->prox;
    else {
        int pos = hash(tamTabela, nodo->chave);
        tabelaHash[pos] = nodo->prox;
    }
    
    if (nodo->prox)
        nodo->prox->ant = nodo->ant;
    
   free(nodo);
}