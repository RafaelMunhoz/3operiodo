#include <stdio.h>

#include "tabelaHash.h"

int main(){
	imprimirDadosAlunos();

    size_t tamTabela;
    int chave;
    char op;
    struct nodoHash** tabelaHash;
    struct nodoHash* nodo;

    scanf("%lu", &tamTabela);
    tabelaHash = gerarTabelaHash(tamTabela);
    scanf("%1c", &op);
    while (1) {
        if (op == 'i') {
            scanf("%d", &chave);
            if (!inserir(tabelaHash, tamTabela, chave))
                printf("Falha ao inserir %d.\n", chave);
        }

        else if (op == 'r') {
            scanf("%d", &chave);
            nodo = buscar(tabelaHash, tamTabela, chave);
            if (!nodo)
                printf("Falha ao remover %d.\n", chave);
            else
                excluir(tabelaHash, tamTabela, nodo);
        }

        else if (op == 'b') {
            scanf("%d", &chave);
            if (buscar(tabelaHash, tamTabela, chave))
                printf("Encontrado %d.\n", chave);
            else   
                printf("Nao encontrado %d.\n", chave); 
        }

        else if (op == 'l') 
            imprimirTabelaHash(tabelaHash, tamTabela);

        else if (op == 'f')
            break;

        scanf("%1c", &op);
    }
    liberarTabelaHash(tabelaHash, tamTabela);
    return 0;
}