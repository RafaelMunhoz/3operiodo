A bilbioteca tabelaHash fornece funções para a manipulação de uma tabela hash estática.

A tabela hash gerada por ela, atraves da funcao gerarTabelaHash, consiste em um vetor de ponteiros para 
nodos (nodoHash* tabela[]) de tamanho tamTabela fornecido pelo usuario. Nesse vetor, o elemento v[i] aponta
para o primeiro elemento de uma lista duplamente encadeada de nodos (struct nodoHash) cujas chaves retornam 
o mesmo valor i quando passadas para a função hash.

Além das funções ja prototipadas no template, foram adicionadas:
    - A função hash, que recebe uma chave (int) e o tamanho da tabela (size_t) e retorna o resto da divisão
    do modulo da chave pelo tamanho da tabela: (hash(chave, tamTabela) = |chave| mod tamTabela). 
    - A função criaNodo, que recebe um chave (int) e aloca memória para um nodo com a chave passada.
