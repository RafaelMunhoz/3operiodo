#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE* arq = fopen("arq.bin", "r");
    if (!arq)
    {
        printf("Impossivel abrir arquivo!\n");
        fclose(arq);
        return 1;
    }
    fseek(arq, 0, SEEK_END);
    long int tam = ftell(arq);

    rewind(arq);
    char *poema = malloc(tam * sizeof(char));
    int pos;
    char car;
    fread(&pos , 4, 1, arq);
    fread(&car, 1, 1, arq);
    while(!feof(arq))
    {
        poema[pos] = car;
        fread(&pos , 4, 1, arq);
        fread(&car, 1, 1, arq);
    }
    fclose(arq);
    printf("%s\n", poema);
    free(poema);
}
