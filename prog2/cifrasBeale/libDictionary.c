#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "libDictionary.h"


struct dicionario* criaDicionario()
{
    struct dicionario* novoDicionario = malloc(sizeof(struct dicionario));
    novoDicionario->inicio = NULL;
    return novoDicionario;
}

struct nodoChaves* criaNodoChaves()
{
    struct nodoChaves *novo = malloc(sizeof(struct nodoChaves));
    novo->prox = NULL;
    novo->codigos = NULL;
    return novo;
}

struct nodoCodigos* criaNodoCodigos()
{
    struct nodoCodigos *novo = malloc(sizeof(struct nodoCodigos));
    novo->prox = NULL;
    return novo;
}

//insere chave de maneira ordenada no dicionario, retorna ponteiro para o nodo dela
struct nodoChaves* insereChave(char novaChave, struct dicionario* dicionario)
{

    //dicionario vazio ou novaChave antes do inicio
    if ((dicionario->inicio == NULL) || (novaChave < dicionario->inicio->chave))
    {
        struct nodoChaves* novo = criaNodoChaves();
        novo->chave = novaChave;
        novo->prox = dicionario->inicio;
        dicionario->inicio = novo;
        return novo;
    }

    struct nodoChaves* aux = dicionario->inicio;

    //acha posicao
    while ((aux->prox != NULL) && (aux->prox->chave <= novaChave))
        aux = aux->prox;

    //chave ja esta no dicionario
    if (aux->chave == novaChave)
        return aux;

    //insere
    struct nodoChaves* novo = criaNodoChaves();
    novo->chave = novaChave;
    novo->prox = aux->prox;
    aux->prox = novo;
    return novo;
}

//insere novo codigo em chave
void insereCodigo(int novoCodigo, struct nodoChaves* nodoChave)
{
    struct nodoCodigos* novo = criaNodoCodigos();
    novo->codigo = novoCodigo;
    if (nodoChave->codigos == NULL)
    {
        nodoChave->codigos  = novo;
        return;
    }

    novo->prox = nodoChave->codigos;
    nodoChave->codigos = novo;
}

struct nodoChaves* buscaChaveCodigo(int buscado, struct dicionario* dicionario)
{
    struct nodoChaves* chaveAtual = dicionario->inicio;
    struct nodoCodigos* codigoAtual;
    
    while (chaveAtual != NULL)
    {
        codigoAtual = chaveAtual->codigos;
        while (codigoAtual != NULL)
        {
            if (codigoAtual->codigo == buscado)
                return chaveAtual;
            codigoAtual = codigoAtual->prox;
        }
        chaveAtual = chaveAtual->prox;
    }
    return NULL;
}


struct nodoCodigos* buscaCodigoChave(char buscado, struct dicionario* dicionario)
{
    struct nodoChaves* chaveAtual = dicionario->inicio;
    while (chaveAtual != NULL)
    {
        if (chaveAtual->chave == buscado)
        {
            //retorna nodo aleatorio da lista de codigos
            struct nodoCodigos* atual = chaveAtual->codigos;
            struct nodoCodigos* solucao = chaveAtual->codigos;
            int n = 2;
            srand(time(NULL));
            while(atual != NULL)
            {
                int aleatorio = rand() % n;
                if (aleatorio == 0)
                    solucao = atual;
                atual = atual->prox;
                n++;
            }
            return solucao;
        }
        chaveAtual = chaveAtual->prox;
    }
    return NULL;
}

void destroiListaCodigos(struct nodoCodigos* inicio)
{
    if (inicio == NULL) return;

    destroiListaCodigos(inicio->prox);
    free(inicio);
    inicio = NULL;
}

void destroiListaChaves(struct nodoChaves *inicio)
{
    if (inicio == NULL) return;
    destroiListaChaves(inicio->prox);
    destroiListaCodigos(inicio->codigos);
    free(inicio);
    inicio = NULL;
}

void destroiDicionario(struct dicionario* dicionario)
{
    destroiListaChaves(dicionario->inicio);
    free(dicionario);
    dicionario = NULL;
}






