#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "libDictionary.h"
#define MAX_LIVRO_CIFRA 50
#define MAX_ARQ_CHAVES 10


//le livro cifra para o dicionario
void leLivroCifra(FILE* livroCifra, struct dicionario* dicionario)
{
    int pos = 0;
    char palavra[MAX_LIVRO_CIFRA];
    struct nodoChaves* nodoChave;

    fscanf(livroCifra, "%s", palavra);
    while(!feof(livroCifra))
    {
        nodoChave = insereChave(tolower(palavra[0]), dicionario);
        insereCodigo(pos, nodoChave);
        pos++;
        fscanf(livroCifra, "%s", palavra);
    }
}

//escreve o dicionario no arquivo de chaves
void escreveArqChaves(FILE* arqChaves, struct dicionario *dicionario)
{
    struct nodoChaves* auxChaves = dicionario->inicio;

    while(auxChaves != NULL)
    {
        //escreve chave
        fprintf(arqChaves, "%c: ", auxChaves->chave);

        //escreve codigos da chave
        struct nodoCodigos* auxCodigos = auxChaves->codigos;
        while(auxCodigos != NULL)
        {
            fprintf(arqChaves, "%d ", auxCodigos->codigo);
            auxCodigos = auxCodigos->prox;
        }
        auxChaves = auxChaves->prox;

        fprintf(arqChaves, "\n");
    }
    fprintf(arqChaves, "\n");
}


//le para o dicionario o arq de chaves
void leArqChaves(FILE* arqChaves, struct dicionario* dicionario)
{
    struct nodoChaves* nodoChaveAtual = dicionario->inicio;
    char lido[MAX_ARQ_CHAVES];

    fscanf(arqChaves, "%s", lido);
    while(!feof(arqChaves))
    {
        //eh uma chave
        if(lido[1] == ':')
            nodoChaveAtual = insereChave(lido[0], dicionario);

        //eh um codigo
        else
            insereCodigo(atoi(lido), nodoChaveAtual);

        fscanf(arqChaves, "%s", lido);
    }
}




