
#include <stdio.h>
#include <stdlib.h>
#include "libDictionary.h"

void codifica(FILE* mensagemOriginal, struct dicionario* dicionario, FILE* mensagemCodificada)
{
    struct nodoCodigos* nodoCodigo;
    char lido;
    fscanf(mensagemOriginal, "%c", &lido);

    while (!feof(mensagemOriginal))
    {
        if(lido == ' ')
            fprintf(mensagemCodificada, "%s ", "-1");
        
        else
        {
            nodoCodigo = buscaCodigoChave(lido, dicionario);

            if (nodoCodigo == NULL)
                fprintf(mensagemCodificada, "%c ", lido);
            
            else    
                fprintf(mensagemCodificada, "%d ", nodoCodigo->codigo);
        }
        fscanf(mensagemOriginal, "%c", &lido);
    }
}
