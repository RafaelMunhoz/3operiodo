#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include "libDecode.h"
#include "libEncode.h"
#include "libKeyFile.h"
#include "libDictionary.h"

int main(int argc, char** argv)
{
    int flagE = 0; int flagD = 0; int flagB = 0; int flagC = 0;
    char* caminhoLivroCifra = NULL;
    char* caminhoMensagemOriginal = NULL;
    char* caminhoSaida = NULL;
    char* caminhoEntrada = NULL;
    char* caminhoArqChaves = NULL;
    int op;

    while ((op = getopt(argc, argv, "edb:m:o:c:i:")) != -1)
    {
        switch(op)
        {
            case 'e':
                flagE = 1;
                break;
            case 'd':
                flagD = 1;
                break;
            case 'b':
                flagB = 1;
                caminhoLivroCifra = optarg;
                break;
            case 'm':
                caminhoMensagemOriginal = optarg;
                break;
            case 'o':
                caminhoSaida = optarg;
                break;
            case 'c':
                flagC = 1;
                caminhoArqChaves = optarg;
                break;
            case 'i':
                caminhoEntrada = optarg;
                break;
            default:
                printf("Uso: \n");
                printf("./beale  -e  -b LivroCifra -m MensagemOriginal -o MensagemCodificada -c ArquivoDeChaves \n");
                printf("./beale  -d  -i MensagemCodificada  -c ArquivoDeChaves  -o MensagemDecodificada \n");
                printf("./beale -d -i MensagemCodificada -b LivroCifra -o MensagemDecodificada \n");
                return 1;
        }
    }
    

    //Codifica mensagem com arquivo de chaves gerado a partir do livro cifra
    if (flagE)
    {
        struct dicionario* dicionario = criaDicionario();
        FILE* livroCifra = fopen(caminhoLivroCifra , "r");
        FILE* mensagemOriginal = fopen(caminhoMensagemOriginal, "r");
        FILE* arqChaves = fopen(caminhoArqChaves, "w");
        FILE* mensagemCodificada = fopen(caminhoSaida, "w");

        if(!livroCifra || !mensagemOriginal || !arqChaves || !mensagemCodificada)
        {
            printf("Erro ao abrir arquivos!\n");
            exit(1);
        }

        leLivroCifra(livroCifra, dicionario);
        escreveArqChaves(arqChaves, dicionario);
        codifica(mensagemOriginal, dicionario, mensagemCodificada);

        fclose(mensagemCodificada);
        fclose(arqChaves);
        fclose(mensagemOriginal);
        fclose(livroCifra);
        destroiDicionario(dicionario);
        printf("Mensagem codificada e arquivo de chaves salvo!\n");
        exit(0);
    }

    //decodifica mensagem a partir do arquivo de chaves
    else if (flagD && flagC)
    {
        struct dicionario* dicionario = criaDicionario();
        FILE* mensagemCodificada = fopen(caminhoEntrada, "r");
        FILE* arqChaves = fopen(caminhoArqChaves, "r");
        FILE* mensagemDecodificada = fopen(caminhoSaida, "w");

        if(!mensagemCodificada || !arqChaves || !mensagemDecodificada)
        {
            printf("Erro ao abrir arquivos!\n");
            exit(1);
        }

        leArqChaves(arqChaves, dicionario);
        decodifica(mensagemCodificada, dicionario, mensagemDecodificada);

        fclose(mensagemDecodificada);
        fclose(arqChaves);
        fclose(mensagemCodificada);
        destroiDicionario(dicionario);
        printf("Mensagem decodificada a partir do arquivo de chaves!\n");
        exit(0);
    }

    //decodifica mensagem a partir do livro cifra
    else if (flagD && flagB)
    {
        struct dicionario* dicionario = criaDicionario();
        FILE* mensagemCodificada = fopen(caminhoEntrada, "r");
        FILE* livroCifra = fopen(caminhoLivroCifra, "r");
        FILE* mensagemDecodificada = fopen(caminhoSaida, "w");

        leLivroCifra(livroCifra, dicionario);
        decodifica(mensagemCodificada, dicionario, mensagemDecodificada);

        fclose(mensagemDecodificada);
        fclose(livroCifra);
        fclose(mensagemCodificada);
        destroiDicionario(dicionario);
        printf("Mensagem decodificada a partir do livro de cifras!\n");
        exit(0);
    }
}