#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "libDictionary.h"
#define MAX_MSG_CODIFICADA 10


void decodifica(FILE* mensagemCodificada, struct dicionario* dicionario, FILE* mensagemDecodificada)
{
    struct nodoChaves* nodoChave;
    char str[MAX_MSG_CODIFICADA];
    fscanf(mensagemCodificada, "%s", str);

    while(!feof(mensagemCodificada))
    {
        //eh espaco
        if (strcmp(str, "-1") == 0)
            fprintf(mensagemDecodificada, "%c", ' ');

        //string lida nao eh um codigo
        else if (!isdigit(str[0]))
            fprintf(mensagemDecodificada, "%s", str);

        else
        {
            nodoChave = buscaChaveCodigo(atoi(str), dicionario);

            //codigo nao esta no dicionario
            if(nodoChave == NULL)
                fprintf(mensagemDecodificada, "%s", str);
        
            else
                fprintf(mensagemDecodificada, "%c", nodoChave->chave);
        } 
        fscanf(mensagemCodificada, "%s", str);
    }
}


