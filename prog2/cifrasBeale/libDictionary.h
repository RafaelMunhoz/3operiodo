#ifndef LIBDICTIONARY_H
#define LIBDICTIONARY_H

#include <stdlib.h>

struct nodoCodigos
{
    int codigo;
    struct nodoCodigos* prox;
};

struct nodoChaves
{
    char chave;
    struct nodoCodigos* codigos;
    struct nodoChaves* prox;
};

struct dicionario
{
    struct nodoChaves* inicio;
};

//cria dicionario vazio
struct dicionario* criaDicionario();

//insere chave de maneira ordenada no dicionario
struct nodoChaves* insereChave(char novaChave, struct dicionario* dicionario);

//insere novo codigo em uma chave
void insereCodigo(int novoCodigo, struct nodoChaves* nodoChave);

//retorna chave que contem o codigo buscado, NULL caso nao ache
struct nodoChaves* buscaChaveCodigo(int buscado, struct dicionario* dicionario);

//retorna um codigo aleatorio da chave buscada, retorna NULL caso nao ache a chave
struct nodoCodigos* buscaCodigoChave(char buscado, struct dicionario* dicionario);

//libera memoria
void destroiDicionario(struct dicionario* dicionario);

#endif