#ifndef LIBKEYFILE_H
#define LIBKEYFILE_H

#include <stdlib.h>
#include "libDictionary.h"


//le livro cifra para dicionario
void leLivroCifra(FILE* livroCifra, struct dicionario* dicionario);

//escreve o dicionario no arquivo de chaves
void escreveArqChaves(FILE* arqChaves, struct dicionario *dicionario);

//le para o dicionario o arq de chaves
void leArqChaves(FILE* arqChaves, struct dicionario* dicionario);

#endif