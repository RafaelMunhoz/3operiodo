#ifndef LIBDECODE_H
#define LIBDECODE_H

#include <stdlib.h>
#include "libDictionary.h"

void decodifica(FILE* mensagemCodificada, struct dicionario* dicionario, FILE* mensagemDecodificada);

#endif
