#ifndef LIBENCODE_H
#define LIBENCODE_H

#include <stdio.h>
#include "libDictionary.h"

void codifica(FILE* mensagemOriginal, struct dicionario* dicionario, FILE* mensagemCodificada);

#endif