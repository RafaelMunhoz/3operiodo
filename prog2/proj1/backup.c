#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 100

//Rafael Munhoz da Cunha Marques GRR20224385

int main(int argc, char **argv)
{
    //trata argumentos linha de comando
    int flagT = 0;
    if (argc > 2)
    {
        printf("Uso: ./backup [-t]\n");
        exit(1);
    }
    if (argc == 2)
    {
        if (strcmp(argv[1], "-t") == 0)
            flagT = 1;
        else    
        {
            printf("Uso: ./backup [-t]\n");
        }
    }

    FILE* bkp = fopen("bkp.bin", "r");
    FILE* saida;
    unsigned int numRegistros, tamRegistro, tamArq;
    char nome[MAX];
    void *tmp;

    //le numero de registros
    fread(&numRegistros, 4, 1, bkp);

    for(int i = 0; i < numRegistros; i++)
    {
        //le tamanho e nome do registro
        fread(&tamRegistro, 4, 1, bkp);
        fscanf(bkp, "%s", nome);

        //pula o '\n'
        fseek(bkp, 1, SEEK_CUR);

        //carrega conteudo do registro para tmp
        tamArq = tamRegistro - strlen(nome) - 1;
        tmp = malloc(tamArq);
        fread(tmp, tamArq, 1, bkp);

        //se flagT, printa nome e tamanho do registro
        if (flagT)
        {
            printf("%s  %d\n", nome, tamArq);
        }
        //se nao, cria arquivo e escreve o registro
        else
        {
            saida = fopen(nome, "w");
            fwrite(tmp, tamArq, 1, saida);
            fclose(saida);
        }
        free(tmp);
    }
    fclose(bkp);
    exit(1);
}